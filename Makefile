all: clean
	@echo "Building Tic-Tac-Toe Game"
	mkdir classes
	javac -d classes/ source/*.java
	@echo "Build Completed"

play: all
	@echo "Starting Game..."
	cd classes/ && java Main

clean:
	rm -rf classes
	@echo "Directory Cleaned"

