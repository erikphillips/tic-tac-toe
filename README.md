# Tic-Tac-Toe Game
Hello! Thanks for downloading my game. 

Currently the game is only able to run within a terminal window. 

I have plans in the future to expand the user interface to include a web version.

The updated game will appear here and on my [website](http://calpoly.edu/~ephill07).


## Installing Tic-Tac-Toe
You can download the game from BitBucket by running the following command:

`git clone https://erikphillips@bitbucket.org/erikphillips/tic-tac-toe.git`

This game depends on the following:

1. [Java version 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

2. [GNU Make version >3.81](http://www.gnu.org/software/make/)


### Compile the Game for Your Machine
To compile the game, run `make` within the game's main directory.

### Decompile Game or Cleanup Directory
To decompile the game or to clean up the directory, run `make clean` within the game's main directory.


## Running the Game
You can run the game by entering `make play` in the terminal within the game's main directory after downloading.


## License and Use
Please do not duplicate or use this code for profit. Please reference my page and information when using this project.

Copyright 2015-2017, Erik Matthew Phillips (ephill07@calpoly.edu). All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions, and the following disclaimer,
   without modification.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

3. The names of the above-listed copyright holders may not be used
   to endorse or promote products derived from this software without
   specific prior written permission.

4. The use of the code in source or binary forms is permitted only 
   for non-profit entities and persons. The code cannot be used for profit.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.