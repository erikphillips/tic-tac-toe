/**
 *  Erik Matthew Phillips
 *  ephill07@calpoly.edu
 *
 *  Copyright, 2015-2017. All rights reserved.
 */

public class Rules {

	public static void printRules() {
		System.out.println("-- Tic-Tac-Toe Rules --");
		System.out.println("Three squares in a line wins");
		System.out.println("(Across row, Along column, Diagonally)");
		System.out.println("Naming one or more players 'computer' will\n"
				+ "assign the player to a computer AI player.");
		System.out.println("-- Enjoy the game! --\n");
	}

}
