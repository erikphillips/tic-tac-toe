/**
 *  Erik Matthew Phillips
 *  ephill07@calpoly.edu
 *
 *  Copyright, 2015-2017. All rights reserved.
 */

public class GameStatus {

	public static int gameStatus(Board board, Player p1, Player p2) {

		char s1 = board.getSquare(0, 0).symbol();
		char s2 = board.getSquare(0, 1).symbol();
		char s3 = board.getSquare(0, 2).symbol();
		char s4 = board.getSquare(1, 0).symbol();
		char s5 = board.getSquare(1, 1).symbol();
		char s6 = board.getSquare(1, 2).symbol();
		char s7 = board.getSquare(2, 0).symbol();
		char s8 = board.getSquare(2, 1).symbol();
		char s9 = board.getSquare(2, 2).symbol();

		if (s1 != ' ' && s2 != ' ' && s3 != ' ')
			if (s1 == s2 && s1 == s3) {
				if (s1 == p1.getSymbol())
					return SupportOpperations.PLAYER1_WIN;
				return SupportOpperations.PLAYER2_WIN;
			}
		if (s4 != ' ' && s5 != ' ' && s6 != ' ')
			if (s4 == s5 && s4 == s6) {
				if (s4 == p1.getSymbol())
					return SupportOpperations.PLAYER1_WIN;
				return SupportOpperations.PLAYER2_WIN;
			}
		if (s7 != ' ' && s8 != ' ' && s9 != ' ')
			if (s7 == s8 && s7 == s9) {
				if (s7 == p1.getSymbol())
					return SupportOpperations.PLAYER1_WIN;
				return SupportOpperations.PLAYER2_WIN;
			}
		if (s1 != ' ' && s4 != ' ' && s7 != ' ')
			if (s1 == s4 && s1 == s7) {
				if (s1 == p1.getSymbol())
					return SupportOpperations.PLAYER1_WIN;
				return SupportOpperations.PLAYER2_WIN;
			}
		if (s2 != ' ' && s5 != ' ' && s8 != ' ')
			if (s2 == s5 && s2 == s8) {
				if (s2 == p1.getSymbol())
					return SupportOpperations.PLAYER1_WIN;
				return SupportOpperations.PLAYER2_WIN;
			}
		if (s3 != ' ' && s6 != ' ' && s9 != ' ')
			if (s3 == s6 && s3 == s9) {
				if (s3 == p1.getSymbol())
					return SupportOpperations.PLAYER1_WIN;
				return SupportOpperations.PLAYER2_WIN;
			}
		if (s1 != ' ' && s5 != ' ' && s9 != ' ')
			if (s1 == s5 && s1 == s9) {
				if (s1 == p1.getSymbol())
					return SupportOpperations.PLAYER1_WIN;
				return SupportOpperations.PLAYER2_WIN;
			}
		if (s3 != ' ' && s5 != ' ' && s7 != ' ')
			if (s3 == s5 && s3 == s7) {
				if (s3 == p1.getSymbol())
					return SupportOpperations.PLAYER1_WIN;
				return SupportOpperations.PLAYER2_WIN;
			}

		if (board.hasOpen())
			return SupportOpperations.ACTIVE;
		return SupportOpperations.TIE_GAME;
	}
}
