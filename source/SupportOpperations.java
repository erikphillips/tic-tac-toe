/**
 *  Erik Matthew Phillips
 *  ephill07@calpoly.edu
 *
 *  Copyright, 2015-2017. All rights reserved.
 */

public class SupportOpperations {

	public static final long COMPUTER_THINKTIME = 250;
	
	public static final int ACTIVE = 0;
	public static final int PLAYER1_WIN = 1;
	public static final int PLAYER2_WIN = 2;
	public static final int TIE_GAME = -1;
	public static final int ROWS = 3;
	public static final int COLS = 3;
	
	public static void thinkTime() {
		try {
			System.out.print("\tThinking.");
			Thread.sleep(SupportOpperations.COMPUTER_THINKTIME);
		} catch (InterruptedException e) { }
		try {
			System.out.print(".");
			Thread.sleep(SupportOpperations.COMPUTER_THINKTIME);
		} catch (InterruptedException e) { }
		try {
			System.out.print(".");
			Thread.sleep(SupportOpperations.COMPUTER_THINKTIME);
		} catch (InterruptedException e) { }
		try {
			System.out.print(".");
			Thread.sleep(SupportOpperations.COMPUTER_THINKTIME);
		} catch (InterruptedException e) { }
		try {
			System.out.print(".");
			Thread.sleep(SupportOpperations.COMPUTER_THINKTIME);
		} catch (InterruptedException e) { }
		try {
			System.out.print(".");
			Thread.sleep(SupportOpperations.COMPUTER_THINKTIME);
		} catch (InterruptedException e) { }
		try {
			System.out.print(".");
			Thread.sleep(SupportOpperations.COMPUTER_THINKTIME);
		} catch (InterruptedException e) { }
		try {
			System.out.print(".");
			Thread.sleep(SupportOpperations.COMPUTER_THINKTIME);
		} catch (InterruptedException e) { }
		System.out.println(" Solution found.");
	}

}