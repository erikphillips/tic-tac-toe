/**
 *  Erik Matthew Phillips
 *  ephill07@calpoly.edu
 *
 *  Copyright, 2015-2017. All rights reserved.
 */

public class MyException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MyException() {
		super();
	}

	public MyException(String message) {
		super(message);
	}

}
