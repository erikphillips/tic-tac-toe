/**
 *  Erik Matthew Phillips
 *  ephill07@calpoly.edu
 *
 *  Copyright, 2015-2017. All rights reserved.
 */

public class Main {

	public static void main(String[] args) {
		System.out.println("Welcome to Tic-Tac-Toe!\n");
		Rules.printRules();
		java.util.Scanner input = new java.util.Scanner(System.in);
		Game.play(Player.makePlayer1(input), Player.makePlayer2(input), input);
		input.close();
	}
}
