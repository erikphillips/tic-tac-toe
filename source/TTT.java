/**
 *  Erik Matthew Phillips
 *  ephill07@calpoly.edu
 *
 *  Copyright, 2015-2017. All rights reserved.
 */

public class TTT implements Drawable {

	private Player p1, p2;
	private Board board;
	private Player currTurn;

	public TTT(Player p1, Player p2) {
		this.p1 = p1;
		this.p2 = p2;
		board = new Board();
		currTurn = p1;
	}

	public boolean take(int row, int col) {
		return board.take(currTurn, row, col);
	}

	public boolean take() {
		return board.take((AIPlayer) currTurn);
	}

	public void draw() {
		System.out.println("\nTic-Tac-Toe Board:");
		board.draw();
	}

	public Player getTurn() {
		return currTurn;
	}

	public void nextTurn() {
		if (currTurn.equals(p1))
			currTurn = p2;
		else
			currTurn = p1;
	}

	public int gameStatus() {
		return GameStatus.gameStatus(board, p1, p2);
	}

	public void reset() {
		board.reset();
	}
}
