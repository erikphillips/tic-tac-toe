/**
 *  Erik Matthew Phillips
 *  ephill07@calpoly.edu
 *
 *  Copyright, 2015-2017. All rights reserved.
 */

public class AIPlayer extends Player {

	public AIPlayer(String name) {
		super(name);
		setAI();
	}

	public boolean take(Board board) {
		return find(board).take(this);
	}

	private Square find(Board board) {
		Square[] arr = new Square[SupportOpperations.ROWS * SupportOpperations.COLS];
		int i = 0;

		for (int r = 0; r < SupportOpperations.ROWS; r++)
			for (int c = 0; c < SupportOpperations.COLS; c++)
				arr[i++] = board.getSquare(r, c);
		String str = "";
		for (i = 0; i < SupportOpperations.ROWS * SupportOpperations.COLS; i++) {
			if (arr[i].taken()) {
				if (arr[i].symbol() == this.getSymbol())
					str += "2";
				else
					str += "1";
			} else
				str += "0";
		}

		String[] rows = new String[SupportOpperations.ROWS];
		rows[0] = str.substring(0, 3);
		rows[1] = str.substring(3, 6);
		rows[2] = str.substring(6);

		String[] cols = new String[SupportOpperations.COLS];
		cols[0] = str.substring(0, 1) + str.substring(3, 4) + str.substring(6, 7);
		cols[1] = str.substring(1, 2) + str.substring(4, 5) + str.substring(7, 8);
		cols[2] = str.substring(2, 3) + str.substring(5, 6) + str.substring(8);

		String[] dia = new String[2];
		dia[0] = str.substring(0, 1) + str.substring(4, 5) + str.substring(8);
		dia[1] = str.substring(2, 3) + str.substring(4, 5) + str.substring(6, 7);

		/* Winning Cases */
		for (i = 0; i < SupportOpperations.ROWS; i++) {
			if (rows[i].equals("220")) {
				if (i == 0)
					return arr[2];
				if (i == 1)
					return arr[5];
				if (i == 2)
					return arr[8];
			}
			if (rows[i].equals("202")) {
				if (i == 0)
					return arr[1];
				if (i == 1)
					return arr[4];
				if (i == 2)
					return arr[7];
			}
			if (rows[i].equals("022")) {
				if (i == 0)
					return arr[0];
				if (i == 1)
					return arr[3];
				if (i == 2)
					return arr[6];
			}
		}

		for (i = 0; i < SupportOpperations.COLS; i++) {
			if (cols[i].equals("220")) {
				if (i == 0)
					return arr[6];
				if (i == 1)
					return arr[7];
				if (i == 2)
					return arr[8];
			}
			if (cols[i].equals("202")) {
				if (i == 0)
					return arr[3];
				if (i == 1)
					return arr[4];
				if (i == 2)
					return arr[5];
			}
			if (cols[i].equals("022")) {
				if (i == 0)
					return arr[0];
				if (i == 1)
					return arr[1];
				if (i == 2)
					return arr[2];
			}
		}

		if (dia[0].equals("220"))
			return arr[8];
		if (dia[0].equals("202"))
			return arr[4];
		if (dia[0].equals("022"))
			return arr[0];
		if (dia[1].equals("220"))
			return arr[6];
		if (dia[1].equals("202"))
			return arr[4];
		if (dia[1].equals("022"))
			return arr[2];

		/* Cases for loosing */

		for (i = 0; i < SupportOpperations.ROWS; i++) {
			if (rows[i].equals("110")) {
				if (i == 0)
					return arr[2];
				if (i == 1)
					return arr[5];
				if (i == 2)
					return arr[8];
			}
			if (rows[i].equals("101")) {
				if (i == 0)
					return arr[1];
				if (i == 1)
					return arr[4];
				if (i == 2)
					return arr[7];
			}
			if (rows[i].equals("011")) {
				if (i == 0)
					return arr[0];
				if (i == 1)
					return arr[3];
				if (i == 2)
					return arr[6];
			}
		}

		for (i = 0; i < SupportOpperations.ROWS; i++) {
			if (cols[i].equals("110")) {
				if (i == 0)
					return arr[6];
				if (i == 1)
					return arr[7];
				if (i == 2)
					return arr[8];
			}
			if (cols[i].equals("101")) {
				if (i == 0)
					return arr[3];
				if (i == 1)
					return arr[4];
				if (i == 2)
					return arr[5];
			}
			if (cols[i].equals("011")) {
				if (i == 0)
					return arr[0];
				if (i == 1)
					return arr[1];
				if (i == 2)
					return arr[2];
			}
		}

		if (dia[0].equals("110"))
			return arr[8];
		if (dia[0].equals("101"))
			return arr[4];
		if (dia[0].equals("011"))
			return arr[0];
		if (dia[1].equals("110"))
			return arr[6];
		if (dia[1].equals("101"))
			return arr[4];
		if (dia[1].equals("011"))
			return arr[2];

		/* Priority squares */
		Square s1 = board.getSquare(0, 0);
		Square s2 = board.getSquare(0, 1);
		Square s3 = board.getSquare(0, 2);
		Square s4 = board.getSquare(1, 0);
		Square s5 = board.getSquare(1, 1);
		Square s6 = board.getSquare(1, 2);
		Square s7 = board.getSquare(2, 0);
		Square s8 = board.getSquare(2, 1);
		Square s9 = board.getSquare(2, 2);

		if (!s1.taken())
			return s1;
		if (!s9.taken())
			return s9;
		if (!s3.taken())
			return s3;
		if (!s7.taken())
			return s7;
		if (!s5.taken())
			return s5;
		if (!s2.taken())
			return s2;
		if (!s8.taken())
			return s8;
		if (!s4.taken())
			return s4;
		if (!s6.taken())
			return s6;

		/* Last resort method to find any remaining open squares. */
		for (Square sq : arr)
			if (!sq.taken())
				return sq;

		/* Final return in case of error in code. */
		throw new MyException("AI ERROR: Computer is unable to find a square to take.");
	}

}
