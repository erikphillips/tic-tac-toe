/**
 *  Erik Matthew Phillips
 *  ephill07@calpoly.edu
 *
 *  Copyright, 2015-2017. All rights reserved.
 */

import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Game {

	public static void play(Player player_1, Player player_2, Scanner input) {
		TTT game = new TTT(player_1, player_2);
		game.draw();

		StringTokenizer tokenizer;
		String token1, token2;
		int row, col;

		while (true) {
			while (game.gameStatus() == SupportOpperations.ACTIVE) {
				if (game.getTurn().equals(player_1)) {
					System.out.println(player_1.getName() + "'s turn:");
					if (player_1.getAI()) {
						SupportOpperations.thinkTime();
						game.take();
						game.nextTurn();
						game.draw();
						continue;
					}
				} else {
					System.out.println(player_2.getName() + "'s turn:");
					if (player_2.getAI()) {
						SupportOpperations.thinkTime();
						game.take();
						game.nextTurn();
						game.draw();
						continue;
					}
				}
				try {
					System.out.print("\tEnter square's row and column to take: ");
					tokenizer = new StringTokenizer(input.nextLine(), " ");
					token1 = tokenizer.nextToken();
					token2 = tokenizer.nextToken();
					row = Integer.parseInt(token1) - 1;
					col = Integer.parseInt(token2) - 1;
					game.take(row, col);
					game.nextTurn();
				} catch (NoSuchElementException e) {
					System.out.println("USER ERROR: Invalid square; Enter: row col");
				} catch (ArrayIndexOutOfBoundsException d) {
					System.out.println("USER ERROR: Invalid square; Enter: row col");
				} catch (NumberFormatException f) {
					System.out.println("USER ERROR: Invalid row or column; Number Format Exception.");
				} catch (MyException g) {
					System.out.println(g.getMessage());
				} finally {
					game.draw();
				}
			}

			if (game.gameStatus() == SupportOpperations.PLAYER1_WIN) {
				System.out.println(player_1.getName() + " Wins!");
				player_1.win();
			} else if (game.gameStatus() == SupportOpperations.PLAYER2_WIN) {
				System.out.println(player_2.getName() + " Wins!");
				player_2.win();
			} else
				System.out.println("Tie Game!");
			System.out.println("Player win count:");
			System.out.println("\t" + player_1.getName() + ": " + player_1.getWinCount());
			System.out.println("\t" + player_2.getName() + ": " + player_2.getWinCount());

			System.out.print("\nPlay again(y/n)? ");
			if (!input.nextLine().equalsIgnoreCase("y"))
				break;

			game.reset();
			game.draw();
		}

		System.out.println("Thank you for playing!");
	}

}
