/**
 *  Erik Matthew Phillips
 *  ephill07@calpoly.edu
 *
 *  Copyright, 2015-2017. All rights reserved.
 */

public class Square implements Drawable {

	private boolean taken;
	private Player player;

	public Square() {
		this.player = null;
		this.taken = false;
	}

	public Square(Player player) {
		this.player = player;
		this.taken = true;
	}

	public boolean take(Player player) {
		if (this.taken)
			throw new MyException("USER ERROR: Square has already been taken.");
		this.taken = true;
		this.player = player;
		return true;
	}

	public boolean taken() {
		return this.taken;
	}

	public char symbol() {
		if (!taken())
			return ' ';
		return player.getSymbol();
	}

	public void draw() {
		if (this.taken)
			System.out.print(" " + player.getSymbol() + " ");
		else
			System.out.print("   ");
	}
}
