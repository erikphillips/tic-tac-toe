/**
 *  Erik Matthew Phillips
 *  ephill07@calpoly.edu
 *
 *  Copyright, 2015-2017. All rights reserved.
 */

public class Board implements Drawable {

	private Square[][] squares;

	public Board() {
		squares = new Square[SupportOpperations.ROWS][SupportOpperations.COLS];
		for (int r = 0; r < SupportOpperations.ROWS; r++)
			for (int c = 0; c < SupportOpperations.COLS; c++)
				squares[r][c] = new Square();
	}

	public boolean take(Player player, int row, int col) {
		return squares[row][col].take(player);
	}

	public boolean take(AIPlayer player) {
		return player.take(this);
	}

	public Square getSquare(int row, int col) {
		return squares[row][col];
	}

	public void reset() {
		for (int r = 0; r < SupportOpperations.ROWS; r++)
			for (int c = 0; c < SupportOpperations.COLS; c++)
				squares[r][c] = new Square();
	}

	public boolean hasOpen() {
		for (Square[] sq : squares)
			for (Square s : sq)
				if (!s.taken())
					return true;
		return false;
	}

	public void draw() {
		int r = 0, c = 0;
		System.out.print("\t    ");
		for (c = 0; c < SupportOpperations.COLS; c++)
			System.out.print((c + 1) + "   ");
		System.out.println();
		for (r = 0; r < SupportOpperations.ROWS; r++) {
			System.out.print("\t   ");
			for (c = 0; c < SupportOpperations.COLS; c++)
				System.out.print("--- ");
			System.out.print("\n\t" + (r + 1) + " ");
			for (c = 0; c < SupportOpperations.COLS; c++) {
				System.out.print("|");
				squares[r][c].draw();
				System.out.print("");
			}
			System.out.print("|\n");
		}
		System.out.print("\t   ");
		for (c = 0; c < SupportOpperations.COLS; c++)
			System.out.print("--- ");
		System.out.println();
	}

}
