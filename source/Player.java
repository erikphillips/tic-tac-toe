/**
 *  Erik Matthew Phillips
 *  ephill07@calpoly.edu
 *
 *  Copyright, 2015-2017. All rights reserved.
 */
	 
import java.util.Scanner;

public class Player {

	private String name;
	private char symbol;
	private int wincount;
	private boolean AI;

	public Player(String name) {
		this.name = name;
		this.symbol = '@';
		this.wincount = 0;
		this.AI = false;
	}

	public String getName() {
		return this.name;
	}

	public void setSymbol(char symbol) {
		this.symbol = symbol;
	}

	public char getSymbol() {
		return this.symbol;
	}

	public void setAI() {
		this.AI = true;
	}

	public boolean getAI() {
		return AI;
	}

	public void win() {
		wincount++;
	}

	public int getWinCount() {
		return this.wincount;
	}

	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (!this.getClass().equals(other.getClass()))
			return false;
		Player p = (Player) other;
		return p.name.equalsIgnoreCase(this.name) && p.symbol == this.symbol;
	}

	public static Player makePlayer1(Scanner input) {
		System.out.print("Enter the name of the first player: ");
		Player player_1 = new Player(input.nextLine());
		player_1.setSymbol('X');
		try {
			if (player_1.getName().substring(0, 8).equalsIgnoreCase("computer")) {
				player_1 = new AIPlayer(player_1.getName());
				player_1.setSymbol('c');
			}
		} catch(StringIndexOutOfBoundsException e) {
			// PASS
		}
		return player_1;
	}

	public static Player makePlayer2(Scanner input) {
		System.out.print("Enter the name of the second player: ");
		Player player_2 = new Player(input.nextLine());
		player_2.setSymbol('O');
		try {
			if (player_2.getName().substring(0, 8).equalsIgnoreCase("computer")) {
				player_2 = new AIPlayer(player_2.getName());
				player_2.setSymbol('C');
			}
		} catch(StringIndexOutOfBoundsException e) {
			// PASS
		}
		return player_2;
	}
}
